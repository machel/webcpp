#!/usr/bin/env bash

DATE_FMT="%Y-%m-%d_%H:%M:%S"
date_beg=$(date +"$DATE_FMT")

OUTFILE="bin/main.out"

OPTIMIZE="
    -g -O0
"

WARN_OPTS="
    -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic
    -Wold-style-cast -Wcast-align -Wunused -Woverloaded-virtual -Wconversion -Wsign-conversion -Wmisleading-indentation
    -Weffc++
"

SRC_FILES=$(\find . -type f -name '*.cpp')

INCLUDE_DIRS="-I."
LIB_DIRS=""
LIB_NAMES=""

if [[ $1 == "run" ]]; then
    $OUTFILE
else
    echo -e ":: Starting bulid procedure\n"
    g++ $WARN_OPTS $OPTIMIZE $INCLUDE_DIRS $LIB_DIRS $LIB_NAMES -o $OUTFILE $SRC_FILES
    echo -e "\n:: End build procedure"
fi

date_end=$(date +"$DATE_FMT")

echo -e "\n=============================================="
echo -e "SUMMARY: "
echo -e "Start: ${date_beg}"
echo -e "End:   ${date_end}"
echo -e "\n\n\n\n"
