WebCpp
======
C++ web development framework.

--------------------------------------------------------------------------------

_Note_
Page designed to be run through the [Doxygen](http://www.doxygen.com) code
documentation engine. It uses [extension of Markdown formatting.](http://www.stack.nl/~dimitri/doxygen/manual/markdown.html).

The settings to generate documentation: Doxyfile.
