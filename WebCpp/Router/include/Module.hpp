/** \file
 *  Module's interface.
 *  External interface of this application module.
 *
 *  \bug No known bugs.
 */

#ifndef WEBCPP_ROUTER_MODULE_HPP
#define WEBCPP_ROUTER_MODULE_HPP


namespace WebCpp {
    class Module;
}


/** Module's namespace.
 *  All module's functionalities live inside it.
 */
namespace WebCpp { namespace Router {  // .....................................


/** Module run mode.
 *  Why, how
 */
enum class RunMode
{
    UNDEF = 0,  ///< not set yet or unknown
    BY_WEBCPP,  ///< WebCpp module will ignite me when it is ignited itself
    MANUALLY,   ///< Client (programmer) must ignite me
    NUM         ///< SPECIAL: number of constants in enum
};
extern const char* const run_mode_descr[];
const char* runModeToString (RunMode run_mode);


/** WebCpp module.
 *  Why, how
 */
class Module
{
  friend class ::WebCpp::Module;

  private:
    RunMode mode;               ///< module's run mode
    static Module instance;     ///< singleton instance
    static bool in_use;         ///< singleton is already in use - first user is exclusive user
    static Module& create (RunMode mode);
  public:
    void printMode ();
};

}}  // .........................................................................

#endif //#- WEBCPP_ROUTER_MODULE_HPP -#//
