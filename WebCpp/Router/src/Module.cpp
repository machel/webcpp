/** \file
 *  \copydoc WebCpp/Router/include/Module.hpp
 *
 *  \bug No known bugs.
 */

#include "../include/Module.hpp"
#include <iostream>

namespace WebCpp { namespace Router {  // .....................................


/** String description of enum class \ref RunMode
 *  \sa runModeToString
 */
constexpr const char* const run_mode_descr[] = {
    "undefined",
    "by WebCpp",
    "manually",
    "SPECIAL: number of constants in enum"
};

/** Return string description of enum class \ref RunMode constant
 *  \sa run_mode_descr
 *  \return constant's description
 */
const char* runModeToString (
    RunMode run_mode    ///< module's run mode constant
) {
    return run_mode_descr[static_cast<int>(run_mode)];
}


Module Module::instance{};
bool Module::in_use = false;

/** Create instance of singleton.
 *  Instance is allocated statically and had program duration. It can be used
 *  only in one place in code and subsequent usages will throw exception.
 *  \return reference to singleton instance
 */
Module& Module::create (
    RunMode mode        ///< module's run mode
){
    if (in_use) {
        throw "nie";
    }
    in_use = true;
    instance.mode = mode;
    return instance;
}

/** Print actual mode.
 *  \warning Temporary.
 */
void Module::printMode ()
{
    std::cout << runModeToString(mode);
}

}}  // .........................................................................
