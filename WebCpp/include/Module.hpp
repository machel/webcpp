/** \file
 *  Module's interface.
 *  External interface of this application module.
 *
 *  \bug No known bugs.
 */

#ifndef WEBCPP_MODULE_HPP
#define WEBCPP_MODULE_HPP

/** Module's namespace.
 *  All module's functionalities live inside it.
 */
namespace WebCpp {  // .........................................................


/** Module run mode.
 *  Why, how
 */
enum class RunMode
{
    UNDEF = 0,  ///< not set yet or unknown
    ONE_TIME,   ///< run once like PHP web app
    DAEMON,     ///< keep in memory, wait for orders on unix socket / windows?
    SERVER,     ///< listen on websocket for requests
    NUM         ///< SPECIAL: number of constants in enum
};
extern const char* const run_mode_descr[];
const char* runModeToString (RunMode run_mode);


/** WebCpp module.
 *  Why, how
 */
class Module
{
  private:
    RunMode mode;               ///< module's run mode
    static Module instance;     ///< singleton instance
    static bool in_use;         ///< singleton is already in use - first user is exclusive user
  public:
    static Module& create (RunMode mode);
    template<typename T, typename E> static T& awakeSubmodule (E mode);
    void printMode ();
};

/** Awake WebCpp submodule for use.
 *
 *  Example:
 *  \code
 *  auto submodRouter = WebCpp::Module::awakeSubmodule<WebCpp::Router::Module>(WebCpp::Router::RunMode::BY_WEBCPP);
 *  \endcode
 */
template<typename T, typename E>
T& Module::awakeSubmodule (
    E mode
){
    return T::create(mode);
}

}   // .........................................................................

#endif //#- WEBCPP_MODULE_HPP -#//
