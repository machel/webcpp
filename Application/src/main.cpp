/** \file
 *  Main file.
 *  Only purpose of this file is to host main function.
 *
 *  \warning Only main function here, nothing more.
 *
 *  \todo continue usage of modules
 *
 *  \bug No known bugs.
 */

// 199711L for C++98 // 201103L for C++11 // 201402L for C++14
#if __cplusplus < 201402L
    #error C++14 compliant compiler needed, aborting
#endif

#include <iostream>
#include "WebCpp/include/Module.hpp"
#include "WebCpp/Router/include/Module.hpp"
#include "Site/Admin/inc/Page.hpp"
#include "Site/Admin/Panel/inc/Page.hpp"

/** Main function.
 *  Main entry point for program. Environment supplies \a argc and \a argv
 *  parameters.
 *
 *  Example:
 *  \code
 *      > ./build.sh
 *      > ./build.sh run
 *  \endcode
 *
 *  \retval 0   Program finished successfuly
 *  \retval 1+  Program finished with errors
 *  \return \c 0-255 Program exit code
 */
int main (
    int argc,       ///< Number of cl arguments
    char* argv[]    ///< Cl arguments array
) {
    using namespace std;

    // ModuleBase
    //    /         \
    // AppModule    PageModule

    auto moduleWebCpp = WebCpp::Module::create(WebCpp::RunMode::ONE_TIME);

    auto submodRouter = WebCpp::Module::awakeSubmodule<WebCpp::Router::Module>(WebCpp::Router::RunMode::BY_WEBCPP);
    // operacja = sciezka (cel-ctrl) + command (method) + data
    // /admin/panel/main/(CMD=show)/(VARA=data1)/(data2)/user/page/main/(CMD=show)/(data1)      ()-mozna pominac
    // /?OP[]={"TARGET":"/admin/panel/main","CMD":"show","VARA":"data1"}

    auto pageAdmin = Site::Admin::Page::create();
    auto subpagePanel = pageAdmin.awakeSubpage<Site::Admin::Panel::Page>();
    //auto ctrlAdminPanelMain = subpagePanel.pickCtrl();
    //submodRouter.defineRoute("/admin/panel/main", ctrlAdminPanelMain);

    //submodRouter.useClArgs(argc, argv);
    //submodRouter.useEnvData(env);
    //submodRouter.ignite();


    return 0;
}
