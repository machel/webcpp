WebCpp Development
==================
Entry/starting point to documentation for application.

WebCpp is developed as application module - as it is intended for normal use.
Application may use other auxiallary modules, whilst helpful in development,
are not needed by WebCpp in normal use.

Each module's documentation is in separate file:

- @ref Application/doc/application.md
- @ref WebCpp/doc/webcpp.md
- @ref WebCpp/Router/doc/router.md

--------------------------------------------------------------------------------

_Note_
Page designed to be run through the [Doxygen](http://www.doxygen.com) code
documentation engine. It uses [extension of Markdown formatting.](http://www.stack.nl/~dimitri/doxygen/manual/markdown.html).

The settings to generate documentation: Doxyfile.
