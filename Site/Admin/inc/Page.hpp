/** \file
 *  Page's interface.
 *  External interface of this application module.
 *
 *  \bug No known bugs.
 */

#ifndef SITE_ADMIN_PAGE_HPP
#define SITE_ADMIN_PAGE_HPP

/** Page's namespace.
 *  All module's functionalities live inside it.
 */
namespace Site { namespace Admin {  // .........................................


/** Admin page.
 *  Why, how
 */
class Page
{
  private:
    static Page instance;   ///< singleton instance
    static bool in_use;     ///< singleton is already in use - first user is exclusive user
  public:
    static Page& create ();
    template<typename T> static T& awakeSubpage ();
};

/** Awake subpage for use.
 *
 *  Example:
 *  \code
 *  auto subpagePanel = Site::Admin::awakeSubpage<Site::Admin::Panel::Page>();
 *  \endcode
 */
template<typename T>
T& Page::awakeSubpage (
){
    return T::create();
}

}}   // ........................................................................

#endif //#- SITE_ADMIN_PAGE_HPP -#//
