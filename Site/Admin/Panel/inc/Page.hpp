/** \file
 *  Subpages's interface.
 *  Module-internal interface of this subpage.
 *
 *  \bug No known bugs.
 */

#ifndef SITE_ADMIN_PANEL_PAGE_HPP
#define SITE_ADMIN_PANEL_PAGE_HPP


namespace Site { namespace Admin {
    class Page;
}}


/** Page's namespace.
 *  All page's functionalities live inside it.
 */
namespace Site { namespace Admin { namespace Panel {  // .......................


/** Site page.
 *  Why, how
 */
class Page
{
  friend class ::Site::Admin::Page;

  private:
    static Page instance;       ///< singleton instance
    static bool in_use;         ///< singleton is already in use - first user is exclusive user
    static Page& create ();
};


}}}  // ........................................................................

#endif //#- SITE_ADMIN_PANEL_PAGE_HPP -#//
