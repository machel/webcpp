/** \file
 *  \copydoc Site/Admin/Panel/inc/Panel.hpp
 *
 *  \bug No known bugs.
 */

#include "../inc/Page.hpp"
#include <iostream>

namespace Site { namespace Admin { namespace Panel {  // .......................


Page Page::instance{};
bool Page::in_use = false;

/** Create instance of singleton.
 *  Instance is allocated statically and had program duration. It can be used
 *  only in one place in code and subsequent usages will throw exception.
 *  \return reference to singleton instance
 */
Page& Page::create (
){
    if (in_use) {
        throw "nie";
    }
    in_use = true;
    return instance;
}


}}}  // ........................................................................
